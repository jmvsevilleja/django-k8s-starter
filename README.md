# Django Kubernetes starter

This is a very basic Django Kubernetes starter project
showcasing a minimal depoyment of django with celery workers
on to a kubernetes cluster.

It was made for a blog post (pending), read the post for an
explanation.

## Requirements to run

To run this project you need a kubernetes cluster with
Traefik installed. Make sure you have DNS records pointing
at the resulting load balancer.

## Setup

If you want to setup the project as is, update the ingress 
[here](kubernetes/ingress.yaml) use to a URL you have already 
pointing at the load balancer, then you can run the following 
commands:

```bash
helm install --name rabbit --values rabbit-values.yaml stable/rabbitmq
helm install --name postgres --values postgres-values.yaml stable/postgresql
sleep 30  # We need to wait for rabbit and postgres to start first
kubectl apply -f kubernetes
```

Go to the relevant URL and the service should be up and running.
